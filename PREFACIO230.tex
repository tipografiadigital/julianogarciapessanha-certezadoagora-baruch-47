\chapterspecial{Posfácio}{}{Peter Pál Pelbart}
 

Não espere o leitor, deste último volume da trilogia sulfurosa de
Juliano Pessanha, qualquer certeza para os tempos presentes. Da primeira
à última de suas linhas, é outra coisa que está em jogo: demolição. Como
Thomas Bernhard, o autor se dá por tarefa denunciar o pacto da universal
hipocrisia que assegura nossa existência cotidiana. E~desvela a
argamassa metafísica que, a cada instante, nos impede de \emph{desabar}.
Mas com isto, paradoxalmente, vemos ruírem um a um todos os personagens
competentes que garantem o ``negócio da administração da vida'', pais,
educadores, psiquiatras, socializadores, homens da cultura --- todos os
que fazem as vezes de carcereiros da vida. Nesse acerto de contas
ilimitado, que o narrador não deixa de ampliar e preterir a cada passo,
não há sombra de ressentimento, mas a força de um diagnóstico que cabe,
a cada um dos textos aqui reunidos, modular conforme seu gênero, seja
ele poético ou ensaístico.

A partir da autobiografia do personagem Gombro (sem dúvida inspirado no
escritor Witold Gombrowicz), temos acesso às estratégias de
sobrevivência que uma criança inventa para contornar a cristalização do
mundo em coisa, para desafiar todos aqueles que fazem da palavra o
instrumento prostituído de uma dissimulação generalizada. Como evitar o
homicídio que nos é proposto desde a mais tenra idade, como driblar o
apagamento do rosto próprio, como fugir à narrativa de si já sempre
terceirizada pelos que nos ``cuidam'' ou nos ``amam''? A resposta que o
autor oferece é poética. Só retomando por conta própria os sufocamentos
e revides (``Passei boa parte de minha vida gritando em túneis, janelas
e becos''), deitando por escrito a percepção precoce da morte e do
infinito, da dor e da ausência. Só assim rompe"-se a linearidade factual
de uma vida, e pode ela ser esburacada pela série de perguntas que a
cadenciaram: ``Há"-alguém"-aí? Há alguma vida verdadeira no planeta? Por
que a assim chamada vida familiar e a assim chamada vida escolar e a
assim chamada vida social trituram a criança possível? Por que
sobrevivem apenas os falsários, os que se identificam com a criança
morta?''

Deleuze dizia que numa literatura dita ``menor'', como a de Kleist,
Kafka, Beckett, Gombrowicz, e diferentemente da literatura dita
canônica, o escritor necessariamente atinge em si seu próprio ponto de
bastardização, ele escreve a partir de seu ponto de subdesenvolvimento,
de seu terceiro mundo, de seu patoá, de seu deserto. Como tornar"-se o
nômade e o imigrado e o cigano e o vidente de sua própria língua?
Juliano Pessanha responde revisitando seu deserto íntimo e dando voz, a
partir de seu exílio interno, às questões e aos gritos soterrados. Mas
ao retomar"-se através de sua narrativa esburacada, evita o risco da
confissão lamurienta: faz da autobiografia um instante de celebração, um
acontecimento jubiloso, uma meditação ziguezagueante em que a palavra e
o abismo passam a pertencer"-se mutuamente, assim como a noite e a vida.

Como se vê, tudo aqui é matéria para a mais sutil filosofia, domínio no
qual o autor passeia com desenvoltura. Mas ele não se autoriza a falar
filosoficamente a partir de um saber competente (que aliás, não lhe
falta), e sim a partir da indigência e da abertura. Podemos compartilhar
menos ou mais de seu enfoque filosófico, o que importa é a acuidade do
diagnóstico, a pertinência com a qual ele ``planta sua inquietude'' e
recusa o ``domingo ontológico'' e seus avatares contemporâneos. Esse
livro enfrenta com coragem poética nossos tempos sombrios de
``mobilização total'', de ``alcoolismo existencial'', da vivência
concebida como ``engorda'' à identidade --- em suma, do que Nietzsche
chamaria de niilismo.

Seria preciso atribuir a Juliano Pessanha aquilo que ele diz de
Heidegger, mas que é próprio de uma linhagem literária do século \versal{XX},
desde Kafka e Musil, até Cioran e Blanchot: a virtude de pôr o mundo em
estado de hesitação, a capacidade de tornar"-se máquina de desfazer
presentidade. Para além ou aquém do filósofo que mais o inspirou (a
britadeira"-Heidegger, ou o cuco maior, como ele diz não sem uma ponta de
comicidade), é preciso reconhecer a voz única que o próprio autor criou
para si, e que se faz ouvir nos textos aqui reunidos, e sobretudo na
extraordinária autobiografia em torno da qual ``giram'', por assim
dizer, os ensaios e poemas que a acompanham, dando"-lhes essa unidade
secreta. Voz sussurrada a partir da morte, do grito e da sublevação.
Nela se aliam o humor e o frêmito, o detalhe prosaico e o enlevo do
pensamento, a causticidade do cronista e a distância consigo mesmo.

Não há heróis, nessa história sem história, apenas essa voz com um ritmo
de tirar o fôlego, capaz de reconquistar o seu ``agora'' sem abrir mão
da errância que lhe deu origem. Pois em meio ao presente de desencanto,
o autor propõe o mais singelo e vulcânico dos gestos: introduzir ``entre
o chão e a cratera'' uma palavra de hesitação, de espera e de
pressentimento.
