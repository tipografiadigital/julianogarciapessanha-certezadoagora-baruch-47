\select@language {brazilian}
\contentsline {part}{Ensaio}{9}
\contentsline {chapter}{Província da escritura}{13}
\contentsline {part}{Heterotanatografia}{23}
\contentsline {chapter}{Introdução: Esse"-menino"-aí }{27}
\contentsline {part}{Ensaio}{59}
\contentsline {chapter}{Natalidade e crise do tempo antropológico }{63}
\contentsline {section}{Bibliografia}{74}
\contentsline {part}{Poemas}{77}
\contentsline {chapter}{atração dos antípodas}{81}
\contentsline {chapter}{lucidez}{83}
\contentsline {chapter}{poema do amor consumado}{85}
\contentsline {chapter}{poema da vida consumada}{87}
\contentsline {chapter}{serpente enrolada}{89}
\contentsline {chapter}{lugares}{91}
\contentsline {chapter}{conjugação}{93}
\contentsline {chapter}{segredo de um amor \textsc {\MakeTextLowercase {I}}}{95}
\contentsline {chapter}{segredo de um amor \textsc {\MakeTextLowercase {\textsc {\MakeTextLowercase {II}}}}}{97}
\contentsline {chapter}{poema cerebral \textsc {\MakeTextLowercase {\textsc {\MakeTextLowercase {III}}}}: (desconstrução da medicina)}{99}
\contentsline {chapter}{jardim}{101}
\contentsline {section}{quase poema: (aforismo vertical antipsíquico)}{101}
\contentsline {chapter}{«Pedagogia» da Perfuração}{103}
\contentsline {section}{Breve suma do S\emph {er e tempo}}{103}
\contentsline {chapter}{Posfácio para uma Trilogia}{111}
\contentsline {section}{}{111}
\contentsline {section}{}{111}
\contentsline {section}{O trem, o entre e o Paradiso terrestre }{111}
\contentsline {section}{Outras referências}{115}
\contentsline {chapter}{Posfácio}{123}
\contentsfinish 
