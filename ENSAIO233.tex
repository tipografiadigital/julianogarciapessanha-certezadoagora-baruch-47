\part{Ensaio} 

\thispagestyle{empty}\vspace*{.4\textwidth}

\epigraph{Não cortem o cordão que liga o corpo à criança do sonho, o cordão astral
à criança aldebarã, não cortem o sangue, o ouro. A~raiz da floração
coalhada com o laço no centro das madeiras negras. \redondo{[…]} Não
talhem a placenta por onde o fôlego do mundo lhe ascende à cabeça.}{\versal{HERBERTO} \versal{HELDER}} 

\chapter{Natalidade e crise do tempo antropológico\footnoteInSection{Heidegger no \emph{Ser e Tempo} pensa apenas a singularização negativa.
A~singularização positiva, ainda que compreendida claramente por ele,
não foi desenvolvida de modo explícito e temático, pois é um assunto
existenciário concernente a cada um de nós singularmente. Por isso, as
análises do ser"-para"-a-morte próprio não são complementadas com análises
do que seria um ser"-para"-o-início próprio: a experiência da incorporação
do nascimento e da retomada (\emph{Wiederholung}) recordativa da
constelação natal que asseguram ao Dasein sua ``estabilidade estendida''
(§\,75) são nomeadas apenas em breves passagens. Qual o afeto portador do
testemunho existenciário onde um homem encontra o lugar de seu destino
(\emph{Schicksal})? Heidegger nos diz muito pouco sobre a alegria e o
júbilo da singularização positiva.} }

\epigraph{\emph{Eu é um outro.}}{\versal{RIMBAUD}} 

Certa vez Kafka escreveu o seguinte: ``É este o problema: há muitos
anos, estando de ânimo baixo, sentei"-me um dia numa encosta do
Laurenziberg. Passei em revista todos os desejos que gostaria de
realizar na vida. Descobri que o mais importante, ou o mais agradável,
seria o de ter chegado à compreensão do sentido da existência (e ---
isto estava forçosamente ligado a ele --- de poder transmiti"-lo aos
outros por escrito), demonstrando que a vida, ainda que mantendo sua
ascensão e seu declínio naturais, deveria ser ao mesmo tempo, e não
menos claramente, entendida como um nada, um sonho, uma obscura
flutuação no espaço…'' Kafka morreu em 1923, aos 41 anos de
idade, e eu sempre fiquei pensando no que teria acontecido a ele caso
tivesse sobrevivido mais alguns anos e tivesse podido ler o \emph{Ser e
Tempo} de Heidegger. Digo isso porque, nessa obra, Heidegger oferece uma
descrição da condição humana bastante próxima do que é dito nas linhas
de Kafka, a saber, que nossa existência não é redutível ao elemento
vivo; que ela é uma relação com o nada e sobre ele flutua num espaço sem
raiz. Foi mesmo a partir da leitura de Heidegger e desse trecho de Kafka
que veio"-me a imagem de uma teia de aranha na noite como uma
imagem"-síntese do acontecer existencial do homem. Ela guarda com rigor a
precariedade e a fragilidade de um ente suspenso no abismo da noite, bem
como o lugar encontrado para não despencar. A~relação com a noite fala
de uma singularização negativa que é universal e implica todos nós aqui,
agora, e em outras culturas. A~singularização positiva diz de como cada
um de nós dependurou"-se nalgum lugar a fim de afastar a constante e
permanente falta de lugar. Esse segundo momento diz respeito à
singularização positiva, pois, ainda que todos lidemos com a
impossibilidade e com a falta de lugar, as possibilitações e os lugares
que encontramos são singulares e dizem de um devir"-fulano e de um
devir"-sicrano.

O analista ou terapeuta, tal como eu o vejo, está interessado nesse
movimento da singularização positiva, isto é, está interessado em
descobrir e em revelar o lugar onde o paciente plantou sua inquietude.
Descobrir o lugar onde plantamos nossa inquietude, ou melhor, onde cada
um de nós plantou a iitquietude, é estar atento às
experiências"-fundantes. Experiências"-fundantes são os
acontecimentos"-cruciais portadores da instanteação da abertura. Pensado
rigorosamente, o próprio termo ``experiência'' não é um termo adequado,
pois ele supõe que já haja alguém capaz de experimentar algo, quando, na
verdade, estamos ainda numa hora anterior, numa hora vaga"-lume que é a
antecâmara de alguém. Não se trata ainda de alguém, mas da transição
eclosiva do ninguém ao alguém. A~psicologia e o modo metafísico comum de
pensar não podem entender essa passagem porque, desconhecendo a
negatividade e a linha da fronteira, pensam apenas experiências, mas não
os acontecimentos"-marcas que operam a alquimia da passagem entre o Fora
e o Dentro em sentido não metafísico. Para sentir"-pensar os
acontecimentos"-híbridos da chegada ao mundo, é preciso ter se educado na
fome da revelação de um rosto e na dor e no júbilo que isso acarreta. O~terapeuta ou analista é aquele homem cujo olhar"-escuta está em condições
de sintonizar e de ``enxergar'' os acontecimentos fundantes inerentes à
constelação da Natalidade. E~Natalidade se diz aqui com ``n'' maiúsculo
para esclarecer que não se trata de natalidade cronológica, mas da mais
íntima duração existencial de cada um de nós. Natalidade diz do que é,
do que foi e do que será no espaço transiente de nosso entre nascimento
e morte. A~Natalidade não diz das experiências possíveis, mas do âmbito
em que elas se dão. Natalidade não são as águas moventes do rio, mas a
constância do leito onde elas passam. O~analista é aquele cuja visão nos
relembra o leito. É~um ver diferente, pois o paciente desconhece as
possibilitações originárias que o lançaram e não tem notícia da
constelação natal que o arremessou junto aos outros e aos entes de um
determinado modo. Tudo isso está encoberto. Há no homem um duplo
esquecimento e um duplo encobrimento. Não apenas o nada e a
impossibilidade são esquecidos, mas também o nascimento ontológico e a
eventualização da passagem. A~normatividade impessoal encarregou"-se já
dessa dupla alienação. O~homem normalizado do impessoal desconhece que é
habitado por essa dupla alteridade e compete ao terapeuta"-analista,
situado no lugar vivo"-morto, lugar estranho e instável entre o
impossível e a eclosão do lance, relembrar ao paciente o que lhe é
próprio, restituí"-lo ao leito. Trabalho ético da amizade e jamais
trabalho de competência teórico"-técnica. O~olho técnico só enxerga na
luz e só produz ofuscamento. É~um olho"-escuta que já pensa saber tudo,
antes mesmo de começar a ver e a escutar o outro. O~amigo, ao contrário,
trabalha na sombra e, protegido pela ignorância, é ainda capaz de
testemunhar e acolher as notícias e as indi"-cações oriundas da
constelação natal. Essa capacidade não é magia nem intuição. Ela é algo
dado desde que se ocupe o lugar mais simples, o lugar humano quando o
humano não está esquecido nem enfiado na ilusão. A~dificuldade do
simples não reside em nenhuma complicação teórica, mas é um assunto
existencial concernente ao poder viver na instabilidade. Esta é a única
dificuldade do simples, e ela é uma questão pessoal para a qual
faculdades de psicologia e instituições psicanalíticas nada têm a
ensinar. As instituições e os seus porta"-vozes, os homens"-do"-instituído,
sempre chamarão de magia e de misticismo tudo aquilo que eles não
conseguem compreender. O~terapeuta, na condição daquele que vê, é quem
nos auxilia a passar da autobiografia, enquanto escrita ilusória do
``eu'' a partir da interpretação pública da vida e do mundo, para a
heterotanatografia, enquanto escrita do outro a partir da morte. Essa
outrificação é o ``si"-mesmo"-próprio'' e ela designa o movimento do
``tornar"-se aquilo que se é'' mostrado por Nietzsche em sua obra e
retomado, em certa medida, por Heidegger no \emph{Ser e
Tempo.}\footnote{Este texto foi lido na Associação Brasileira de Daseinanalyse, em São
Paulo (21.11.2001).}  É apenas a partir dessa outrificação que
podemos pensar a constância e a íntima duração existencial do si"-mesmo
no entre do nascimento"-e-morte.

Elias Canetti, que em toda sua obra pensou o tema do si"-mesmo e da
singularização positiva, escreveu:

``A pergunta, a terrível pergunta: um homem realmente se transforma?

No \emph{Banquete}, Platão responde afirmativamente como se tivesse
acabado de ler Heráclito. Os homens portam durante toda vida o mesmo
nome, diz ele, e eles são outros, tudo neles e dentro deles é sempre
diferente.

Não acredito nisso, não estou absolutamente certo disso. Sei onde sou o
mesmo que sempre fui. É~difícil até mesmo ver onde se é diferente.''

A questão da dignidade e da legitimidade ontológica do nome e da
conversão do anonimato impessoal em nome próprio é a questão da acolhida
da alteridade em nós. Tornamo"-nos quem já somos quando relembramos as
saudações natais do Dentro. O ``agora'' dessa rememoração é o que chamo
de segundo nascimento ou amigamento do leito. Nesse ``agora'', o homem é
capaz de soletrar as próprias datas e dizer o lugar de onde vem.
Trata"-se de um dizer inesgotado e inesgotável, um dizer eminentemente
antifilosófico e somente a literatura e a poesia podem portar o ``saber
psicanalítico'' concernente ao ``sopro das origens''. E, mais uma vez,
cumpre lembrar que não se pensa a origem em sentido cronológico, mas
como o perdurante. Perdurante é o lugar onde plantamos a inquietude.
Esse lugar não é nenhuma reedição de um fundamento metafísico ou
substancial. Esse lugar é o sopro do arremesso que somos e pensá"-lo ao
modo do meramente dado é tentar situar"-se fora dele (arremesso), o que
constitui a íntima tentação da metafísica. O~lance"-arremesso é o que
perdura na travessia até a morte, e o lugar a partir do qual se desdobra
nosso acontecer não pacifica nem aquieta, pois nenhum homem morre
suficientemente velho a ponto de, numa só vida, ter desdobrado todas as
suas dobras (Michaux). O~dizer ético do analista é aquele que, ao
traduzir o dito para o revelado no dito, aponta sempre na direção do
sopro e não na de uma medida pública. O~sopro oriundo do lugar persiste
até o fim biológico, pois o lugar é atravessado pelo nada de tal maneira
que não há pacificação da turbulência nalgum pretenso domingo
ontológico. O~dizer do analista não domestica a turbulência, apenas
favorece o amigamento do lugar. Esse amigamento não cria nem inventa
nada, simplesmente acolhe o ofertado na constelação natal, de tal modo
que a liberdade, diferentemente da sua ilusória imagem moderna, é apenas
a guarda do necessário. Tanto mais um homem é livre quanto mais ele
existe a reboque do sopro da eclosão presentificante, a reboque do
necessário. O~discurso moderno e o dito pós"-moderno da criação e da
autoinvenção humanas atêm"-se unicamente às vivências, e o fundamento
escondido do primado das vivências e dos choques (W\,Benjamin) na alta
modernidade é a questão da tecnificação da própria natalidade e da
conversão do humano em não nascido; em mero artefato disponível para o
funcionamento da máquina"-mundo.

Já nos anos 10 e 20, Max Weber notava que o homem moderno só podia
morrer cansado pois ele, ao longo de sua vida, já não tinha mais
condições para desdobrar os enigmas que a vida lhe colocara. Isso
significa que a temporalização antropológica do tempo começava a entrar
em crise, cedendo lugar a um tipo de temporalização alheia ao humano. Se
o tempo humano implica numa ligação constante com a ausência e a eclosão
da marca (eventualização), o tempo tecno"-científico fecha"-se para esse
elemento da transcendência ek"-stática e se fixa única e excmsivamente na
dimensão da presença, conhecendo apenas o ekstase caído das
atualizações. Vale dizer que o avanço da modernidade realiza uma
profanação do tempo antropológico, exilando o homem na mera duração
cronológica. O~homem se exila no atual, na pura atualidade do presente
caído, e perde a relação com a pulsação e com a duração existencial.
Nesta hora, a humanidade começa a deslizar na direção de um ``Movimento
Falso'' (P\,Handke), pois, confinada no atual, só pode embriagar"-se de
progresso e o progresso enquanto melhora das condições atuais apenas
aprofunda o aprisionamento no mesmo. A~vida humana perde o sentido, pois
se desamiga do enraizamento no leito, e, doravante, privado de marcas, o
homem se dependura apenas nas vivências e no contágio do instantâneo,
perdendo, assim, a dignidade ontológica do próprio nome.

No tempo em que Max Weber redigiu suas considerações, esses dois modos
de temporalização conviviam e o segundo ainda não tinha invadido todos
os nichos do acontecer humano.\footnote{O nascimento da psicanálise pode ser entendido à luz do contexto da
dupla temporalização. Freud compreende, em parte, o tempo antropológico,
mas pensa a cura pskanalítica a partir do primado da duração cronológica
e da boa"-ordem"-do"-tempo"-civilizado. Acrescente"-se a isso que todas as
teorias psicanalíticas enquanto pretensões de esgotar e ``dizer
teoricamente'' a constelação natal fundam"-se na vontade de poder e traem
o próprio espírito psicanalítico, pois este aponta sempre para o
impossível de qualquer teorização.}  A modernidade não
tinha, ainda, atingido o seu ápice"-acabamento e o modo de
sentir"-compreender tecno"-científico ainda não era hegemônico nem
totalitário. Havia zonas tradicionais resistentes à invasão da
``Mobilização Total'' (E\,Jünger). A~própria constelação familiar
enquanto lugar privilegiado da natalidade e da chegada ainda era uma
província de resguardo e de relativa alteridade em relação ao mundo. A~família ainda não era uma mera célula reprodutiva do sistema"-mundo. Vale
dizer que o homem (Dasein) ainda tinha um lugar ôntico e uma identidade
ôntica e, se ele se apropriasse de si mesmo, então ele se apropriava de
algo diferente da ordem do mundo, de algum conteúdo particular, pois,
para falar metafisicamente, ele ainda tinha alguma particularidade (T\,Adorno) e não era mero ``ersatz'' do universal consumado. Embora a crise
do tempo antropológico se acelerasse, o humano ainda podia ser
compreendido à luz da finitização da dupla alteridade e ele estava,
ainda, em condições de desbancar a impessoalidade pela outrificação no
si"-mesmo"-próprio. Visitado pela angústia e pelo chamado (\emph{Ser e
Tempo}, §§\,55--57), ele podia reapropriar"-se das marcas da constelação
natal e ``reintroduzir"-se'' no contínuo e na íntima duração de um
destino singular (\emph{idem}, §§\,74--75). Com o avanço da extinção do
tempo antropológico, o destino dá lugar ao desvio e, ao recuar para
``Fora'' na visita da angústia, o homem já não encontra marca alguma que
o repita: o homogêneo perdurante de um destino dá lugar ao aleatório do
Desvio. Segundo a expressão lapidar de Gottfried Benn: ``eu olhava em
mim mesmo, mas o que eu via era espantoso, eram dois fenômenos, a
sociologia e o vazio''. Sem nenhuma marca, onde estaria, agora, o ponto
onde o amálgama do tempo se desdobra? Onde então o evento da eclosão de
um homem? A partir dos anos 30, e, mais nitidamente, a partir do final
da segunda guerra e, no Brasil, a partir dos anos 50, começam a pipocar
esse tipo de perguntas.\footnote{No Brasil, essas questões não foram formuladas. Até onde sei,
intelectuais tanto de direita quanto da esquerda estavam hipnotizados
com a modernização e o progresso, e jamais questionaram o quadro como um
todo. A~recepção de Heidegger, da Escola de Frankfurt e outros, dá"-se a
partir dos anos 60--70, quando tudo isso já virou mero assunto de
competência acadêmica.}  É a partir de então, quando
a espiral do progresso se torna avassaladora, que os nichos mais
renitentes se tornam cada vez mais ameaçados e a mobilização total
penetra todas as esferas da vida se estendendo até mesmo ``à criança no
berço. Ela está ameaçada como todo mundo e, aliás, ainda mais
fortemente'' (Jünger, \emph{A~Mobilização Total}).

Quando a armação tecnológica invade a própria província familiar
enquanto lugar da natalidade, então desaparece a possibilidade de
qualquer contato humano. Um contato humano real depende de um lugar
opaco e insaturado onde o recém chegado teria sido ao menos sonhado por
alguém e de que os gestos e as palavras que o saudassem guardassem o
inaudito e o inédito daquela chegada. Se os saudadores e os anfitriões
são hipermetafísicos e cessou qualquer interpelação do desconhecido, a
criança passa inteiramente desapercebida: todos estarão de prontidão
para o trabalho do parto, para o trabalho da amamentação, para o
trabalho do amor, para o trabalho higiênico, para o trabalho do carinho
necessário e para o trabalho da educação. O~recém"-chegado converte"-se
num nunca"-visto, pois a sociedade funcional estendeu seu braço sinistro
até o interior dos pais e das mães já, agora, escorregados para dentro
da área da mera funcionalidade maquinal. Se os saudadores são incapazes
de recuar para aquém do todo e já não sentem a dor ou qualquer
nostalgia, nem o pressentimento de que o todo é falso, então é porque
eles entronizaram já o próprio universo técnico e se autocompreendem a
partir das narrativas da função"-mãe e da função"-pai. Nessas condições, a
chegada é a tal ponto abafada e sufocada pela hipersaturação de gestos e
significações prescritas que a criança não encontra mais nenhum
acontecimento vaga"-lume para se dependurar, nem lugar ou olhar algum que
ainda preserve a pulsação da existência. O~Dasein, que é o tataraneto do
último"-homem nietzscheano, é cuspido para um aí gelado e teatralizado, e
herda uma argamassa hipernomeada e pronta que, enquanto produção pública
e universal, nivela e achata toda província natal. Assim, a saudação do
Dentro tende a se tornar uma mesma cena padrão na medida em que o grande
ministério da realidade, na condição de comunicação e informação
universalmente difundidas, agenciam a existência de cabo a rabo,
incluindo até mesmo os seus começos.

Nessa altura, com a essência humana fixada na mera positividade, não há
mais como restituir o tempo humano. O~homem continua visitado pela
angústia, mas a única resposta que ele pode dar é dependurar"-se nas
vivências e nos signos sociais: quando a quantidade de entulhos jogado
nas águas do rio é tão imensa que fecha, por deposição contínua, o
próprio acesso aoleito, tudo o que resta é superestimular"-se (turismo,
sexo, esporte, \emph{workshops} etc.) nas velocidades de uma superfície
e leveza rarefeitas. Vivência, eu ainda não defini o termo, é uma
espécie de alcoolismo existencial. Na vivência, o vivido está sob o
inteiro controle do ``eu''. Ela (vivência) é posta de antemão dentro de
um script, e ela visa --- ``nietzscheanamente'' --- um aumento do poder
uma vez que o pós"-vivido, na condição de autorrelato, apenas engorda a
identidade metafisicamente determinada. Nessas condições terminais de
crise do tempo antropológico, a psicanálise e as terapias tendem a
desaparecer, pois desaparece o seu objeto, a saber, o sujeito humano.
Que outra história teria o analista para mostrar a não ser a história
anti"-histórica de uma imensa destituição? Qual é, então, agora, a nossa
certeza do agora, a não ser uma sabedoria do
nunca?\footnote{Sem a parte da sombra, o acontecer humano fica abortado e a existência
se converte em simulacro. Ao homem desta hora, o homem"-terminal, já não
há mais caminho para trás. Resta"-lhe apenas aninhar"-se na desolação e
descer até o lugar de reconciliação com o fundante, lugar onde lembramos
que nosso corpo pertence à terra e nossa palavra ao mistério.} 

Como percebeu lucidamente Elias Canetti, ``a partir de um certo ponto, a
história não era mais real. Sem que se percebesse, toda a humanidade
subitamente abandonou a realidade: tudo que aconteceu desde então
supostamente não foi verdadeiro; mas nós supostamente não percebemos.
Nossa tarefa agora seria encontrar esse ponto, enquanto não o
localizarmos, estaremos condenados a mergulhar na nossa destruição
presente''.

\section{Bibliografia}

\begin{itemize}
\item
  \versal{ADORNO}, T\,\& \versal{HORKHEIMER}, M\,\emph{Dialética do Esclarecimento}. Rio
  de Janeiro, Jorge Zahar, 1985.
\item
  \versal{BENJAMIN}, W\,``Experiência e Pobreza'' e ``O Narrador''. \emph{Magia e
  Técnica, Arte e Política}. São Paulo, Brasiliense, 1987.
\item
  \_\_\_\_\_\_. ``Sobre alguns Temas em Baudelaire''. \emph{Charles
  Baudelaire, um Lírico no Auge do Capitalismo}. São Paulo, Brasiliense,
  1989.
\end{itemize}
\begin{itemize}
\item
  \versal{CANETTI}, E\,\emph{A~Consciência das Palavras}. São Paulo, Companhia
  das Letras, 1990.
\item
  \_\_\_\_\_\_. \emph{Die Provinz des Menschen}. München/Wien, Carl
  Hanser Verlag, 1993.
\item
  \_\_\_\_\_\_. \emph{Massa e Poder}. São Paulo, Melhoramentos, 1983.
\item
  \_\_\_\_\_\_. \emph{Nachträge aus Hampstead}. Frankfurt am Main,
  Fischer, 1999.
\item
  \versal{DUARTE}, A\,``Autonomia e Liberdade: ainda há Tempo?''. Palestra
  proferida em setembro de 2001, no Centro Cultural Banco do Brasil, Rio
  de Janeiro.
\item
  \versal{HEIDEGGER}, M\,\emph{Introducción a la Filosofía}. Madrid, Cátedra,
  1999.
\item
  \_\_\_\_\_\_. \emph{Língua de Tradição e Língua Técnica}. Lisboa,
  Vega, 1995.
\end{itemize}
\begin{itemize}
\item
  \_\_\_\_\_\_. \emph{Nietzsche. Metafísica e Niilismo}. Rio de Janeiro,
  Relume"-Dumará, 2000.
\item
  \_\_\_\_\_\_. \emph{Sein und Zeit}. Tübingen, Max Niemeyer, 1984.
\item
  \versal{JÜNGER}, E\,\emph{L'état Universel suivi de la Mobilisation Totale}.
  Paris, Tel Gallimard, 1990. Tradução brasileira: \emph{A~Mobilização
  Total} de Vicente Sampaio. (Não publicada.)
\end{itemize}
\begin{itemize}
\item
  \versal{KAFKA}, F\,\emph{Contos, Fábulas e Aforismos}. Rio de Janeiro,
  Civilização Brasileira, 1993.
\item
  \versal{SIMMEL}, G\,\emph{El Individuo y la Libertad. Ensayos de Crítica de la
  Cultura}. Barcelona, Península, 1986.
\item
  \versal{SLOTERDIJK}, P\,\emph{Essai d'Intoxication Volontaire}. Paris,
  Calmann"-Lévy, 1999.
\item
  \versal{VERNIK}, E\,\emph{El Otro Weber}. Buenos Aires, Colihue, 1996.
\end{itemize}


