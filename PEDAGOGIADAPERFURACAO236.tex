\chapterspecial{«Pedagogia» da Perfuração}{}{}
 \epigraph{Minha agitação é a deste mar --- extensão sem fim, movimento sem limites
\mbox{---,} mas meu segredo consiste em que pertenço a duas realidades. Porque
caminho pela rua Corrientes, em Buenos Aires, justamente depois de ter
jantado no restaurante Sorrento. E~ao mesmo tempo estou ali, no pleno,
no pleno e agitado mar! Sacudido nos espaços agitados… estou em
Corrientes e ao mesmo tempo estou nos mais negros abismos
interplanetários: só no espaço! Acabo de jantar bastante bem e estou
lançado no infinito como um grito…}{\versal{WITOLD} \versal{GOMBROWICZ}} 

\section{Breve suma do S\emph{er e tempo}}

Num livro anedótico de autores ingleses, o nome Heidegger designa uma
máquina de procedência alemã destinada a perfurar a substância. A~correção da anedota --- afinal, a destruição da ontologia da coisa é um
projeto explícito de \emph{Ser e Tempo} --- poderia, entretanto,
esconder a verdadeira potência desta britadeira, que, na sua obsessão
pérfuro"-desrealizante, põe em ``estado de hesitação'' não apenas este ou
aquele modo da presentidade (coisa, instrumento), mas a presença como
tal. Isso significa que tanto a instrumentabilidade
(\emph{Zuhandenheit}), como o meramente subsistente
(\emph{Vorhandenheit}), são modos da presentidade e que, em ambos, o ser
já resultou como presença (\emph{Anwesen}). Ora, Heidegger não está
interessado em se deter nesta faixa onde a presença já vigora, mas em
perfurar a faixa ela mesma, a fim de encontrar o elemento ou, melhor
dito, \emph{o movimento} do qual ela resulta. Vale dizer: o que é que
possibilita a presentificação?

Com isso não se pretende, obviamente, atenuar a diferença que há entre
essas duas chaves da presentidade: encontrar o ente como instrumento é
radicalmente diverso de encontrá"-lo como coisa. Quase um terço das
páginas de \emph{Ser e Tempo} tratam da instrumentabilidade, e é
precisamente a ideia de um acesso instrumental ao ente que permite a
crítica da ontologia da coisa (perfuração da substância) e um primeiro
deslocamento em relação ao predomínio metafísico da metáfora do olhar.
Não é pouco afirmar que todo pensamento ocidental, de Parmênides até
Husserl, esqueceu o fenômeno do mundo e permaneceu preso ao modelo do
ver teorético. Não é pouco abrir um território, até então insuspeitado,
a partir do qual o acesso ao ente pode ser concebido fora do regime do
logocentrismo (da ideia de que é o juízo que abre o ente; de que sua
descoberta é a exposição num discurso do tipo S é P). Não é pouco,
também, deixar para trás toda uma série de imagens metafísicas do
homem"-vidente e do mundo como sistema"-de"-coisas ``em nome'' do Dasein
agente"-compreensor e do mundo como sistema de tarefas; afinal, se o
velho espectador apenas discernia as \emph{propriedades} de algo real,
agora o Dasein envolvido e expectante se \emph{apropria} de
instrumentos, pois já compreendeu ali um sentido e articulou uma
possibilidade. Mas se nada disso é pouco, e certamente não o é, o que
falta então? Não é a noção de instrumentabilidade uma noção central, a
ponto de Heidegger sempre a ela retornar, até mesmo na página final de
\emph{Ser e Tempo}? Sem dúvida que ela é importante, mas não é central.
O~verdadeiro núcleo de \emph{Ser e Tempo} está, ainda, em outro lugar,
e, por mais que o inventário acerca do potencial revolucionário da noção
de instrumentabilidade pudesse seguir até o tedioso, ele seria sempre um
prelúdio, provavelmente ainda tímido, de uma revolução mais profunda:
\emph{``Ser e Tempo'' não é apenas uma máquina de desrealização (crítica
da ontologia da coisa), mas uma máquina de desfazer toda e qualquer
presentidade}. Isso quer dizer que, tanto na visualização do meramente
subsistente quanto na apropriação de um instrumento, está pressuposto um
encontro cuja possibilitação permanece impensada e que, em ambos os
casos, ainda que de maneira diversa, já fomos concernidos e atingidos
pelo que \emph{é}. Qual a raiz desta não indiferença em relação \emph{ao
que é}? São questões deste tipo que formam o núcleo de \emph{Ser e
Tempo}, um livro inacabado que aponta já para um lugar que não mais
pertence à metafísica. Ora, se também no instrumento já vigora algo que
é, resta então perguntar como a máquina de desfazer presentidade vai
perfurar o instrumento.

Uma das consequências da noção de instrumentabilidade é, precisamente, a
superação da questão tradicional do conhecimento. Quando se pergunta
pela maneira como se dá a instauração do comércio gnoseológico
(sujeito/mundo, espírito/real etc.), encobre"-se o ``fato'' de que esse
comércio já aconteceu e que o ente intramundano já está descoberto. Isso
significa que eu não pré"-subsisto, encapsulado em minha natureza
específica, até o momento em que se abre alguma janela na consciência.
Não; eu já estou sempre ``lá fora'', junto aos entes, e é só na medida
desse comércio acontecido que eu mesmo posso ser. \emph{Mas se o ente
intramundano já está descoberto é porque deve vigorar alguma espécie de
luz. De que luminosidade se trata?} Posto que não é nenhuma ``luz
ocular'' (dos sentidos), nenhuma luz divina ou racional, trata"-se,
tão"-somente, da luminosidade do sentido (da significação). Vale dizer
que há uma cena significativa, uma trama (sistema de remissões), cujo
fio me é familiar e ao qual este ou aquele ente pertence ou não
pertence, cabe ou excede, tem lugar ou exorbita. Se um pigmeu, cuja
sorte o tivesse privado dos antropólogos e demais especialistas do
``homem'', se deparasse, na selva, com um telefone celular, subtraída a
hipótese de que ele pudesse fazer daquilo um uso qualquer e insuspeitado
(projetar possibilidades para o ente), diríamos que ele encontrou algo
absurdo. O~telefone excede à rede conformativa daquele mundo; não
pertence ao conjunto das remissões que o pigmeu ``entende''. Mas algo só
excede se pensamos numa relação: é só sobre a base de um mundo como
``conjunto'' de significações que um ente particular pode ter ou não
sentido. O~absurdo ratifica que o sentido já vigora e que um certo mundo
aconteceu --- ele não é ausência de sentido, mas contra"-sentido sob a
base do sentido acontecido. Essa base é exatamente c mundo na acepção
existencial, um lugar iluminado a partir do qual o ente pode ganhar um
ser. ``Aí'', nesse lugar, o ente é. Está descoberto: foi compreendido e
apropriado em função de uma rede de significatividade que, segundo
Heidegger, constitui a própria estrutura do mundo. ``Em todo
instrumento, o mundo já está 'aí'' (§\,18). Ora, estávamos perseguindo o
instrumento a fim de perfurá"-lo e encontramos o fenômeno do mundo.
\emph{Seria então o mundo esse lugar iluminado onde acontece o sentido;
finalmente, a província última em que poderíamos assentar e edificar?}
Constituiria o fenômeno do mundo uma espécie de positividade
alternativa? Aquilo que a nossa máquina negativa, a
britadeira"-Heidegger, não conseguiria atravessar? A resposta é não. Esse
mundo já aberto e por nós ocupado, essa cena legível e iluminada dentro
da qual nos movemos todos os dias, não é senão a vertigem de uma
\emph{fuga} e o resultado de um \emph{encobrimento}. A~significatividade
em que nos sustentamos (mundo) esconde precisamente a irrupção original
do mundo. É~apenas quando a significatividade quebra e se retira (§\,40),
quando, rigorosamente, já não posso encontrar nenhum ente (instrumento,
coisa subsistente, e mesmo ``eu'') que o mundo se mostra como mundo e
que emerge o ``quem'' do Dasein.

Mundo (e não o estoque das coisas), na acepção existencial anteriormente
explicitada, não é algo dado de uma vez por todas. A~abertura iluminada
onde encontro o ente pode ausentar"-se. E~não é um ausentar"-se qualquer,
mais ou menos provável, mas algo que já acercou desde sempre o Dasein. O~Dasein é o ente que existe, porque é aquele ente que mantém ligação com
esse ausentar"-se. Por mais que o Dasein insista na segurança do mundo e
confie nessa ``evidência'', mais ele recalca a possibilidade daquele
ausentar (possibilidade de não mais estar aí), encontrando"-se, portanto,
sempre em relação a ele. Isso equivale a dizer que o ser"-no"-mundo não
evoca um assentamento no necessário ou um alojamento em algo permanente
(Beaufret), mas, ao contrário, é precisamente o que falta e o que não
está dado. Por isso, quando se afirma, como geralmente acontece, que o
homem em \emph{Ser e Tempo} é o ser"-no"-mundo, se esquece de acrescentar
que isso não lhe é dado e que o ser"-no"-mundo deve ser lido como um
ter"-de"-chegar"-ao"-mundo e um ter"-de"-manter"-se"-aí"-no"-mundo. É~apenas
resistindo à possibilidade da impossibilidade (ser para a morte) que o
Dasein mantém"-se no interior da significatividade de um mundo, embora
essa resistência nunca supere ou apague a possibilidade de não mais
estar aí.

Nesse ponto torna"-se claro até onde nos conduz a britadeira"-Heidegger:
sua ontologia fundamental não oferece mais nenhuma fundamentação última
no sentido da metafísica, mas aponta para a negatividade radical que nos
atravessa. Somos um constante transbordamento do mundo e nos situamos
precisamente na linha sísmica, entre o chão e a cratera. Vale. dizer
que, doravante, todos os modos de estar"-no"-mundo devem ser lidos como
derivações, no sentido específico de afastamento, recalcamento e
encobrimento da região fronteiriça. É~o próprio pensamento, portanto,
que sofre uma metamorfose, pois ele, longe de ser a busca de um
fundamento inabalável, implica agora num poder permanecer em trânsito
constante pela linha da fronteira. E é preciso, talvez, uma espécie de
pulmão"-anfíbio a fim de interrogar que trânsito e que movimento é esse
que faz a passagem do abismo a casa e que, bifurcando, vincula as
possibilitações ao impossível. Esse trânsito, diz Heidegger, é o tempo.
Sua determinação primária, o futuro, é o nada. Isso significa que a
própria ``decisão'' pelo ser (o residir na casa) é o tempo. É~um tipo
específico de temporalização do tempo (o mais básico). Tanto o encontro
do instrumento quanto o do meramente dado são modos, são ``figuras'',
mais derivadas, da temporalização do tempo. Ambas são legítimas, desde
que não esqueçam sua genealogia e procedência. De nada adiantaria
insistir que, na chave instrumental, o homem pode, agora; responder quem
é?, que horas são?, e onde está?, não mais a partir da insipidez da
cronologia e dos ``agoras'', mas da tensão da vida prática e do
desassossego de suas esperas, se seguimos ignorando o que esse outro
tempo pressupõe. (Que ele já é resultado e não origem.) Mais originário
que esse homem concreto (habitante da espessura cotidiana) é a
finitização do Dasein nele. Quando responde que horas são?, quem, onde e
quando agora?, (as questões inesgotáveis de Claudel e Beckett) e ---
incrivelmente! --- acredita em suas respostas, quando espera por um
emprego, diz as ``suas'' datas, sente medo de uma doença, glorificando
assim a hora positiva, não faz mais do que produzir o seu sintoma,
encobrindo temporalizações mais originárias. Foi o próprio Heidegger
quem, no parágrafo 72 de \emph{Ser e Tempo}, ao falar do nascimento e do
ser"-para"-o-começo, abriu a possibilidade de pensarmos para aquém, tanto
do tempo vazio e abstratamente infinito dos agoras (intratemporalidade,
\emph{Innerzeitlichkeit}) quanto do tempo mundano das ocupações
(temporalidade inautêntica). Pensar o ser"-para"-o-início e o modo da
chegada ao mundo é poder se apropriar daquilo mesmo que, pela primeira
vez, eclodiu contra a noite da ausência. Trata"-se de uma porta que
Heidegger deixou aberta e que permite, para além do próprio \emph{Ser e
Tempo} (das análises da historicidade autêntica) pensarmos numa
singularização positiva e no tema da constância do si"-mesmo. Se um tal
pensamento, que desce até a indigência na qual algo nos foi confiado,
continuar sendo taxado de ``jargão da autenticidade'', então é
necessário que essa expressão equivocada se aplique também ao que a
psicanálise nos legou de mais radical.
